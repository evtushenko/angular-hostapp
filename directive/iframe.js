(function () {
    'use strict';

    angular.module('hostapp.iframe', [])
        .directive('hostappIframe', function () {
            return {
                restrict: 'A',
                scope: {
                    pageNo: '=',
                    pageSize: '=',

                    apiUrl: '=',
                    apiToken: '='
                },
                link: function ($scope, $element) {
                    var postMessage = function (message) {
                        $element[0].contentWindow.postMessage(JSON.stringify(message), '*');
                    };

                    $scope.$watch('pageNo', function () {
                        postMessage({pageNo: $scope.pageNo});
                    });
                    $scope.$watch('pageSize', function () {
                        postMessage({pageSize: $scope.pageSize});
                    });
                    $scope.$watch('apiUrl', function () {
                        postMessage({apiUrl: $scope.apiUrl});
                    });
                    $scope.$watch('apiToken', function () {
                        postMessage({apiToken: $scope.apiToken});
                    });
                }
            }
        });
})();