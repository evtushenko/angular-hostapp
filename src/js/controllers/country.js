(function () {
    'use strict';

    angular.module('hostapp.controllers.country', [])
        .controller('countryController', [
            '$scope', '$http', '$routeParams', 'tableConfig',
            function ($scope, $http, $routeParams, tableConfig) {
                _.extend($scope, {
                    country: {},
                    error: ''
                });

                var ctrl = this;
                _.extend(ctrl, {
                    load: function () {
                        $http.get(ctrl.getApiUrl(), ctrl.getApiConfig())
                            .success(function (country) {
                                $scope.country = country;
                                $scope.error = '';
                            })
                            .error(function () {
                                $scope.country = {};
                                $scope.error = 'Error loading data';
                            });
                    },
                    getApiUrl: function () {
                        return [
                            tableConfig.apiUrl,
                            '/',
                            $routeParams.id
                        ].join('');
                    },
                    getApiConfig: function () {
                        return {
                            withCredentials: false,
                            headers: {
                                'Content-Type': 'application/json',
                                'X-REST-Token': tableConfig.apiToken
                            }
                        };
                    }
                });

                $scope.$watch(function () {
                    return [
                        tableConfig.pageSize,
                        tableConfig.pageNo,
                        tableConfig.apiUrl,
                        tableConfig.apiToken
                    ].join('/');
                }, function () {
                    ctrl.load();
                });
            }]);
})();