(function () {
    'use strict';

    angular.module('hostapp.controllers.countries', [])
        .controller('countriesController', [
            '$scope', '$http', 'tableConfig',
            function ($scope, $http, tableConfig) {
                _.extend($scope, {
                    countries: [],
                    error: ''
                });

                var ctrl = this;
                _.extend(ctrl, {
                    load: function () {
                        $http.get(ctrl.getApiUrl(), ctrl.getApiConfig())
                            .success(function (countries) {
                                $scope.countries = countries;
                                $scope.error = '';
                            })
                            .error(function () {
                                $scope.countries = [];
                                $scope.error = 'Error loading data';
                            });
                    },
                    getApiUrl: function () {
                        return [
                            tableConfig.apiUrl,
                            '?',
                            'perPage=' + tableConfig.pageSize,
                            '&',
                            'page=' + tableConfig.pageNo
                        ].join('');
                    },
                    getApiConfig: function () {
                        return {
                            withCredentials: false,
                            headers: {
                                'Content-Type': 'application/json',
                                'X-REST-Token': tableConfig.apiToken
                            }
                        };
                    }
                });

                $scope.$watch(function () {
                    return [
                        tableConfig.pageSize,
                        tableConfig.pageNo,
                        tableConfig.apiUrl,
                        tableConfig.apiToken
                    ].join('/');
                }, function () {
                    ctrl.load();
                });
            }]);
})();