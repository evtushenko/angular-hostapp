(function() {
    'use strict';

    angular.module('hostapp.controllers', [
        'hostapp.controllers.index',
        'hostapp.controllers.about',
        'hostapp.controllers.contact',
        'hostapp.controllers.country',
        'hostapp.controllers.countries'
    ]);
})();