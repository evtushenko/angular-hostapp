(function() {
    'use strict';

    angular.module('hostapp', [
        'hostapp.templates',
        'hostapp.controllers',
        'ngRoute'
    ])
        .config(['$routeProvider', function ($routeProvider) {
            $routeProvider.when('/', {
                templateUrl: 'app/index.html',
                controller: 'indexController',
                controllerAs: 'indexController'
            });
            $routeProvider.when('/about', {
                templateUrl: 'app/about.html',
                controller: 'aboutController',
                controllerAs: 'aboutController'
            });
            $routeProvider.when('/contact', {
                templateUrl: 'app/contact.html',
                    controller: 'contactController',
                    controllerAs: 'contactController'
            });
            $routeProvider.when('/countries', {
                templateUrl: 'app/countries.html',
                controller: 'countriesController',
                controllerAs: 'countriesController'
            });
            $routeProvider.when('/country/:id', {
                templateUrl: 'app/country.html',
                controller: 'countryController',
                controllerAs: 'countryController'
            });
            $routeProvider.otherwise({
                redirectTo: '/'
            });
        }])

        .value('tableConfig', {
            apiUrl: 'http://graviton-api-szkb-bap.nova.scapp.io/entity/country',
            apiToken: undefined,
            pageNo: 1,
            pageSize: 10
        })

        .run(['$window', '$rootScope', 'tableConfig', function ($window, $rootScope, tableConfig) {
            angular.element($window).bind('message', function (event) {
                try {
                    var data = JSON.parse(event.originalEvent.data);
                    if (_.has(data, 'apiUrl')) {
                        tableConfig.apiUrl = data.apiUrl;
                    }
                    if (_.has(data, 'apiToken')) {
                        tableConfig.apiToken = data.apiToken || undefined;
                    }
                    if (_.has(data, 'pageNo')) {
                        tableConfig.pageNo = parseInt(data.pageNo) || 1;
                    }
                    if (_.has(data, 'pageSize')) {
                        tableConfig.pageSize = parseInt(data.pageSize) || 10;
                    }

                    $rootScope.$apply();
                } catch (error) {
                    console.error(error, event);
                }
            });
        }]);
})();