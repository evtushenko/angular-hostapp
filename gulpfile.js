/* global require */
/* global __dirname */
var gulp    = require('gulp');
var uglify  = require('gulp-uglify');
var ngtpl   = require('gulp-angular-templatecache');
var concat  = require('gulp-concat');
var order   = require('gulp-order');
var connect = require('gulp-connect');

gulp.task('app-tpl', function () {
    return gulp.src(__dirname + '/src/templates/**/*.html')
        .pipe(ngtpl({
            module: 'hostapp.templates',
            root: 'app',
            standalone: true
        }))
        .pipe(gulp.dest(__dirname + '/src/js'));
});
gulp.task('app-css', function () {
    return gulp.src(__dirname + '/src/css/**/*.css')
        .pipe(order())
        .pipe(concat('app.css'))
        .pipe(gulp.dest(__dirname + '/public/assets'));
});
gulp.task('app-js', ['app-tpl'], function () {
    return gulp.src(__dirname + '/src/js/**/*.js')
        .pipe(order([
            'config.js',
            'app.js',
            'services/**/*.js',
            'directives/**/*.js',
            'controllers/**/*.js',
            '**/*.js',
            'templates.js'
        ]))
        .pipe(concat('app.js'))
        .pipe(uglify())
        .pipe(gulp.dest(__dirname + '/public/assets'));
});

gulp.task('vendors-js', function () {
    return gulp.src([
        'bower_components/lodash/dist/lodash.min.js',
        'bower_components/jquery/dist/jquery.min.js',
        'bower_components/bootstrap/dist/js/bootstrap.min.js',
        'bower_components/angular/angular.min.js',
        'bower_components/angular-route/angular-route.min.js'
    ])
        .pipe(concat('vendors.js'))
        .pipe(gulp.dest(__dirname + '/public/assets/'));
});
gulp.task('vendors-css', function () {
    return gulp.src([
        'bower_components/angular/angular-csp.css',
        'bower_components/bootstrap/dist/css/bootstrap.min.css'
    ])
        .pipe(order())
        .pipe(concat('vendors.css'))
        .pipe(gulp.dest(__dirname + '/public/assets/'));
});


gulp.task('watch', ['build'], function () {
    gulp.watch(__dirname + '/src/templates/**/*.html', [
        'app-tpl'
    ]);
    gulp.watch(__dirname + '/src/js/**/*.js', [
        'app-js'
    ]);
    gulp.watch(__dirname + '/src/css/**/*.css', [
        'app-css'
    ]);
});
gulp.task('server', ['watch'], function () {
    connect.server({
        root: 'public',
        port: 9000
    });
});


gulp.task('vendors', ['vendors-js', 'vendors-css']);
gulp.task('app', ['app-js', 'app-css']);

gulp.task('build', ['vendors', 'app']);
gulp.task('default', ['build']);